# README #

Proyecto para controlar un plato indexador con un motor paso a paso. Se utiliza una placa Arduino Uno para el control, con el shield para agregar una pantalla LCD y seis teclas como interface de usuario. Para el control de potencia se utiliza una placa con el integrado TB6065. EL proyecto original se presento en la revistra Digital Machinist en el verano de 2013 a 2014 y se continúo en esta discución en el foro de Digital Machinist que puede accederse en este [enlace](https://bbs.homeshopmachinist.net/threads/62292-Step-Indexer-Article). El proyecto fué mejorado por Gary Liming y publicado en su pagina web (http://www.liming.org/millindex).

### Novedades en la versión 3.0 ###

Novedades en la versión 3.0

* Se mejoró la traducción y los mensajes de la interfaz de usuario.
* Se agregó la posibilidad de un inicio de giro externo por un terminal digital (PIN 12).
* Se agregó la confirmación de final de posicionamiento cuando el giro se realiza por un pedido externo (PIN 13).
